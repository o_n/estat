options mprint;

%let home_dir = /folders/myfolders/estat/;

%include "&home_dir.appId.mac";
%put appId = &appId.;

libname sds "&home_dir.sds";


%macro read_statdb(id=);

    filename statxml url "http://statdb.nstac.go.jp/api/1.0b/app/getStatsData?appId=&appId.%nrstr(&)statsDataId=&id.";
    filename statmap "&home_dir.input/dataInf_&id..map";
    libname statxml xml xmlmap=statmap;

    proc copy in = statxml out = sds;
        select dataInf_&id.;
    run;

%mend read_statdb;

%read_statdb(id=0000030001);
%read_statdb(id=0000030128);


